#!/usr/bin/env php
<?php

require_once 'config/pi.php';
require_once 'vendor/autoload.php';
require_once 'lib/functions.php';

Logger::configure($GLOBALS['WORK_DIR'] . '/config/logger.php');
$log = Logger::getLogger('default');

if (isset($argv[1])) {
    $id = $argv[1];
} else {
    $id = getenv('TR_TORRENT_ID');
}

if ('' == $id) {
    $log->error('No torrent ID provided.');
    exit(1);
}

// Connect to the database
$db = new PDO($GLOBALS['PDO_DSN']);

// Stop the torrent seed & get the files
exec("transmission-remote {$GLOBALS['HOST_PORT']} -t {$id} -S");
exec("transmission-remote {$GLOBALS['HOST_PORT']} -t {$id} -i | grep Hash | awk '{print $2}'", $hash, $out);
exec("transmission-remote {$GLOBALS['HOST_PORT']} -t {$id} --files | grep -i 'mp4\|mkv\|avi' | cut -c 35-", $files, $rtn);
$hash = strtoupper(trim($hash[0]));

// Get the record of the file out of the database
$stmt = $db->query("SELECT t_show as show, pub_date, t_title FROM magnet WHERE hash='{$hash}'");
$row = $stmt->fetch(PDO::FETCH_ASSOC);


if (count($files) > 0) {
    foreach($files as $f) {

        if (!file_exists($DOWNLOAD_DIR . '/' .$f)) {
            $log->info($f . ' doesn\'t seem to exist.');
            continue;
        }

        $tsize = filesize($DOWNLOAD_DIR . '/' . $f);

        // If the file is too small we don't want it
        // Also, since in 32 bit, filesize returns a negative number for
        // files over 2GB, we're checking between 0 and min... Should work
        // for both 32 bit and 64 bit
        if ($tsize < $GLOBALS['MIN_FILE_SIZE'] && $tsize >= 0) {
            $log->info($f . ' too small to be bothered with');
            continue;
        }

        $ext = pathinfo($f, PATHINFO_EXTENSION);
        $tf = basename($f);

        $info = false;

        if (!empty($row['show'])) {
            $info = parseTitleName($row['t_title'], $row['show']);
            if ($info === false) {
                $info = TvdbLookup($row['show'], $row['pub_date']);
            }
        }

        // Well, shit. This must be a non RSS feed driven download.  Brute force the bitch.
        if ($info === false) {
            $log->info('Calling parseTorrentName. TvdbLookup & parseTitleName must have failed.');
            $info =  parseTorrentName($tf);

            // If title isn't set, then retry passing in full path name
            if (!isset($info['title'])) {
                $log->info('Title not set. Retrying...');
                $info = parseTorrentName($f);

                // If title STILL isn't set, bail...
                if (!isset($info['title'])) {
                    $log->error('No title found off torrent file ' . $f . '. Next please...');
                    continue;
                }
            }
        }

        // Uppercase our title words
        $info['title'] = ucwords($info['title']);

        if (isset($info['season'])) {  // TV Shows
            // If year is set, put it in the title
            if(isset($info['year'])) {
                $info['title'] = $info['title'] . ' (' . $info['year'] . ')';
            }
            $dir = getTVPath($info);
            $file = getTVFileName($info, $ext);
        } elseif (isset($info['year'])) {  // Movies
            $dir = getMoviePath($info);
            $file = getMovieFileName($info, $ext);
        } else {  // Don't know what the fuck this is so I better exit before I do something stupid
            $log->error('Not sure what this file(' . $f . ') is. Moving on...');
            continue;
        }

        // Make our directories
        exec ("mkdir -p {$GLOBALS['SYNC_DIR']}/{$dir}", $out, $rtn);

        if ($rtn != 0) {
            $log->error('Unable to create directory ' . $dir);
            exit(1);
        }

        // Now copy the file over
        $count = 0;
        do {
            $rf = addcslashes($f, "() '");
            $rf = str_replace('\(', '\\(', $rf);
            $rf = str_replace('\)', '\\)', $rf);
            $rf = str_replace("\'", "\\'", $rf);

            exec("/usr/bin/rsync -sq --no-R --no-implied-dirs {$DOWNLOAD_DIR}/{$rf} {$GLOBALS['SYNC_DIR']}/{$dir}/{$file}", $out, $rtn);
            $outString = implode($out, "\n");
            $log->info('Rsync for ' . $file . ': ' . $outString);
            $count++;
        } while ($rtn != 0 && $count < 4);
    }
} else {
    $log->info('No video files found!');
    exit(1);
}

// Now remove and delete the torrent
if ($rtn == 0) {
    exec("transmission-remote {$GLOBALS['HOST_PORT']} -t {$id} --remove-and-delete");
} else {
    foreach ($out as $l) {
        echo $l, "\n";
    }
}

// Finis
echo 'Done', "\n";