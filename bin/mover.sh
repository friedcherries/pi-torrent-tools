#!/usr/bin/env bash

# Get our execution directory location
EXE_PATH=$(dirname "${0}")
EXE_PATH=$(cd "${EXE_PATH}" && pwd)
BASE_PATH=$(dirname "${EXE_PATH}")

# Check to see if the lock file exists. If so, exit
if [ -f /tmp/mover.lock ]; then
    echo "Lock file in place. Exiting..."
    exit 0
fi

# No lock file exists so drop it in
echo "Adding our lock file"
touch /tmp/mover.lock

# Now check for any finished downloads and process them
for ID in `transmission-remote -l | grep Done | grep '100%' | awk '{print $1}'`
do
    echo "Looking at transmission id ${ID}"
    ${BASE_PATH}/cron-download-complete.php $ID
done

# Remove our lock file
echo "Removing our lock file"
rm /tmp/mover.lock