<?php

// Set default timezone
date_default_timezone_set('America/Chicago');

// Datasource for sqlite 3 file. Rename it from -template
$PDO_DSN = 'sqlite:/usr/local/pi/db/torrent.db';

// Your new.showrss.info RSS feed
$RSS_FEED = 'YOUR RSS FEED';

// Where Transmission is downloading to
$DOWNLOAD_DIR = 'TORRENT DOWNLOAD DIRECTORY';

// Where you are rsyncing your files to
$SYNC_DIR = 'FINAL DESTINATION';

// TV directory
$SMB_TV_ROOT = 'TV ROOT DIR';

// Movie directory
$SMB_MOVIE_ROOT = 'MOVIE ROOT DIR';

// Where did you install these tools
$WORK_DIR = 'INSTALL DIR';

// Minimum file size for a file to get copied after download
$MIN_FILE_SIZE = 100 * 1024 * 1024; // Size in MB * 1024 * 1024

// Leave this blank unless you change away from the default port
$HOST_PORT = '';  // localhost:9091

// Your TVDB API KEY
$TVDB_APIKEY = '';

// TVDB API Endpoint
$TVDB_API_URI = 'https://api.thetvdb.com';

// SHOWRSS SUPPORT
$SHOWRSS = true;
