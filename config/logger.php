<?php

return array(
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderRollingFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date{Y-m-d H:i:s} %logger %-5level %msg%n'
                )
            ),
            'params' => array(
                'file' => '/usr/local/pi-torrent-tools/logs/pitts.log',
                'maxFileSize' => '10MB',
                'maxBackupIndex' => 5,
            ),
        ),
    ),
    'rootLogger' => array(
        'appenders' => array('default'),
        'level' => 'DEBUG',
    ),
);