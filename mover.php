#!/usr/bin/env php
<?php

function slash_it($f) {

    $rf = addcslashes($f, "() '");
    $rf = str_replace('\(', '\\(', $rf);
    $rf = str_replace('\)', '\\)', $rf);
    $rf = str_replace("\'", "\\'", $rf);

    return $rf;
}

require_once 'config/pi.php';

$download_dir = $argv[1];
$destination_dir = $argv[2];

foreach (new DirectoryIterator($download_dir) as $fileInfo) {
    if($fileInfo->isDot()) continue;
    $f = $fileInfo->getFilename();

    $dof = slash_it($download_dir . '/' . $f);
    $def = slash_it($destination_dir . '/' . $f);

    exec("/usr/bin/rsync -sq --no-R --no-implied-dirs {$dof} {$def}", $out, $rtn);
}