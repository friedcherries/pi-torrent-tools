<?php

function TvdbLookup($show, $pub_date) {

    if ($GLOBALS['TVDB_APIKEY'] == '') {
        return false;
    }

    $headers = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    ];

    $client = new GuzzleHttp\Client([
        'base_uri' => $GLOBALS['TVDB_API_URI']
    ]);

    $data = ['apikey' => $GLOBALS['TVDB_APIKEY']];
    try {
        $resp = $client->request('POST', '/login', [
            'json' => $data,
            'headers' => $headers
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }

    $auth = json_decode($resp->getBody());
    $headers['Authorization'] = 'Bearer ' . $auth->token;

    try{
        $resp = $client->request('GET', '/search/series?name=' . urlencode($show), ['headers' => $headers]);
        $show_data = json_decode($resp->getBody());
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }

    $id = $show_data->data[0]->id;

    $pdate = new DateTime($pub_date);

    try {
        $resp = $client->request('GET', '/series/' . $id . '/episodes/query?firstAired=' . $pdate->format('Y-m-d'), ['headers' => $headers]);
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }

    $episode = json_decode($resp->getBody());
    $info = [];
    $info['season'] = $episode->data[0]->airedSeason;
    $info['episode'] = $episode->data[0]->airedEpisodeNumber;
    $info['title'] = $show;

    return $info;
}

function parseTitleName($t_title, $show) {

    $info = [];
    $info['title'] = $show;

    $yes = preg_match('/\(\d{4}\)/', $t_title, $matches);

    if (1 == $yes) {
        $info['year'] = $matches[0];
    }

    $yes = preg_match('/\d+x\d+/', $t_title, $matches);

    if (1 == $yes) {
        list($info['season'], $info['episode']) = explode('x', $matches[0]);
    } else {
        $info = false;
    }

    return $info;
}

function parseTorrentName($torrent) {
    $patterns = [
        'resolution' => '/(([0-9]{3,4}p))[^M]/',
        'fulldate' => '/\d{4}\.\d{2}\.\d{2}/',
        'year' => '/([\[\(]?((?:19[0-9]|20[01])[0-9])[\]\)]?)/',
        'episodeplus' => '/([Ss]?([0-9]{2}))[Eex]?([0-9]{2})|[\.]([0-9]{3})[\.]|\.?([0-9]{1,2})x([0-9]{2})\.?/',
        'quality' => '/(?:PPV\.)?[HP]DTV|(?:HD)?CAM|B[rR]Rip|TS|(?:PPV)?WEB-?DL(?: DVDRip)?|H[dD]Rip|DVDRip|DVDRiP|DVDRIP|CamRip|W[EB]B[rR]ip|[Bb]lu[Rr]ay|DvDScr|hdtv/',
        'codec' => '/xvid|XviD|XViD|x264|X264|x265|X265|H264|h\.?264|h\.265|H\ 265|HEVC\ 265/',
        'audio' => '/MP3|DD5\.?1|Dual[\- ]Audio|LiNE|DTS|AAC(?:\.?2\.0)?|AC3(?:\.5\.1)?/',
        'group' => '/(- ?([^-]+(?:-={[^-]+-?$)?))$/',
        'region' => '/R[0-9]/',
        'extended' => '/EXTENDED/',
        'hardcoded' => '/HC/',
        'proper' => '/PROPER/',
        'repack' => '/REPACK/',
        'container' => '/MKV|AVI/',
        'widescreen' => '/WS/',
        'website' => '/^(\[ ?([^\]]+?) ?\])/',
        'language' => '/(rus\.eng)|ENG/'
    ];
    $info = [];

    foreach ($patterns as $key => $i) {
        preg_match($i, $torrent, $matches);
        $matched = count($matches);

        if ( $matched > 0) {
            switch ($key){
                case 'fulldate':
                    $episode = substr($matches[0], 0, 10);
                    $episode = str_replace('.','', $episode);
                    $episode = str_replace('-', '', $episode);
                    $info['season'] = '01'; // This is lame. it means all episodes are in Season 01 folder. Plex won't like that...
                    $info['episode'] = $episode;
                    $repl = $matches[0];
                case 'year':
                    $len = strlen($matches[0]);

                    if ($len == 6) {
                        $year = substr($matches[0], 1, 4);
                    } elseif ($len == 5) {
                        $year = substr($matches[0], 0, 4);
                    } else {
                        $year = $matches[0];
                    }

                    $info[$key] = $year;
                    $repl = $matches[0];
                    break;
                case 'episodeplus':
                    $size = count($matches);
                    if (5 == $size) {
                        $info['season'] = substr($matches[4],0, 1);
                        $info['episode'] = substr($matches[4], 1, 2);
                        $repl = $matches[0];
                    } elseif (4 == $size) {
                        $info['season'] = $matches[2];
                        $info['episode'] = $matches[3];
                        $repl = $matches[0];
                    } elseif (7 == $size) {
                        $info['season'] = $matches[5];
                        $info['episode'] = $matches[6];
                        $repl = $matches[0];
                    }
                    break;
                case 'resolution':
                    $info[$key] = $matches[2];
                    $repl = $matches[1];
                    break;
                default:
                    $info[$key] = $matches[0];
                    $repl = $matches[0];
                    break;
            }

            $torrent = preg_replace($i, '..', $torrent);
        }
    }

    $torrent_a = strstr($torrent, '..', true);

    if ($torrent_a != '') {
        $torrent = $torrent_a;
    }

    $torrent_a = strstr($torrent, '  ', true);
    if ($torrent_a != '') {
        $torrent = $torrent_a;
    }

    $torrent = str_replace('.', ' ', $torrent);
    $torrent = str_replace('-', ' ', $torrent);
    $torrent = str_replace('_', ' ', $torrent);
    $torrent = trim($torrent);
    $info['title'] = $torrent;
    return $info;
}

function getTVPath($info) {
    global $log;

    if (strlen($info['episode']) < 2) {
        $info['episode'] = '0' . $info['episode'];
    }

    if (strlen($info['season']) < 2) {
        $info['season'] = '0' . $info['season'];
    }

    $dir = $GLOBALS['SMB_TV_ROOT'] . '/' . $info['title'] . '/Season ' . $info['season'] . '/';

    $log->info('TV Directory: ' . $dir);

    $dir = addcslashes($dir, "() ':");
    return $dir;
}

function getTVFileName($info, $ext) {
    global $log;

    if (strlen($info['episode']) < 2) {
        $info['episode'] = '0' . $info['episode'];
    }

    if (strlen($info['season']) < 2) {
        $info['season'] = '0' . $info['season'];
    }

    $file = $info['title'] . ' - S' . $info['season'] . 'E' . $info['episode'] . '.' . $ext;

    $log->info('TV File name: ' . $file);

    $file = addcslashes($file, "() ':");
    return $file;
}

function getMoviePath($info) {
    return addcslashes($GLOBALS['SMB_MOVIE_ROOT'] . '/', '() :');
}

function getMovieFileName($info, $ext) {
    global $log;

    $file = $info['title'] . ' (' . $info['year'] . ').' . $ext;

    $log->info('Movie Name: ' . $file);

    $file = addcslashes($file, "() ':");
    return $file;
}
