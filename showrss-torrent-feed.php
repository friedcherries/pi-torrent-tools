#!/usr/bin/env php
<?php

include_once 'config/pi.php';

$no_date_check = false;

if (isset($argv[1])) {
    $RSS_FEED = $argv[1];
}

$feed = file_get_contents($RSS_FEED);
if (false === $feed) {
    $feed = file_get_contents($RSS_FEED);

    if (false === $feed) {
        echo "GOD DAMN SHIT MOTHER FUCKER\n";
        exit;
    }
}

$db = new PDO($GLOBALS['PDO_DSN']);
$xml = simplexml_load_string($feed);
$namespaces = $xml->getDocNameSpaces(true);

foreach ($xml->channel->item as $t) {

    $yes = preg_match('/\d+\-\d+\-\d+/', $t->title, $matches);
    if (1 == $yes) {
        $pdate = $matches[0];
    } else {
        $pubDate = new DateTime($t->pubDate);
        $pubDate = $pubDate->sub(new DateInterval('P1D'));
        $pdate = $pubDate->format('Y-m-d');
    }

    $link = $t->link;
    $tv = $t->children($namespaces['tv']);
    $hash = $tv->info_hash;
    $show = $tv->show_name;

    $stmt = $db->query("SELECT count(1) FROM magnet WHERE hash='{$hash}'");

    if ($stmt->fetchColumn() == 0) {
        // Add magnet link to torrent download
        exec("transmission-remote {$GLOBALS['HOST_PORT']} --add {$link}", $output, $return_code);

        // Implemented quoting
        $show = $db->quote($show);
        $t_title = $db->quote($t->title);

        if ($return_code == 0) {
            $db->query("INSERT INTO magnet (hash, t_show, pub_date, filename, t_title)
                        VALUES ('{$hash}', {$show},'{$pdate}','{$link}', {$t_title})");
            echo "Queued and inserted: {$show} ({$hash})\n";
        } else {
            foreach ($output as $l) {
                echo $l, "\n";
            }
        }
    }
}