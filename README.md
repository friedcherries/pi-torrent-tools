# pi-torrent-tools #

This is just a collection of PHP scripts to automate and organize torrent downloads and video file organizations.

### External Software Requirements ###

* SQLITE 3
* PHP-CLI 5.6
* [Transmission](https://www.transmissionbt.com/)

### How do I get set up? ###

* Sqlite 3

    `apt-get install sqlite3`

* PHP Setup

    `apt-get install php5`

    `apt-get install php5-sqlite`

* Transmission

    `apt-get install transmission`

* Composer
    `https://getcomposer.org/download/`

### Don't forget to run composer install after each clone or update! ###

### Who do I talk to? ###
* [Michael D. Culey](mailto:mike@friedcherries.org)