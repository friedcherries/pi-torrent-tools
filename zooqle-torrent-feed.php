#!/usr/bin/env php
<?php

include_once 'config/pi.php';

$no_date_check = false;

if (isset($argv[1])) {
    $RSS_FEED = $argv[1];
}

$feed = file_get_contents($RSS_FEED);
if (false === $feed) {
    $feed = file_get_contents($RSS_FEED);

    if (false === $feed) {
        echo "GOD DAMN SHIT MOTHER FUCKER\n";
        exit;
    }
}

$db = new PDO($GLOBALS['PDO_DSN']);
$xml = simplexml_load_string($feed);
$namespaces = $xml->getDocNameSpaces(true);

foreach ($xml->channel->item as $t) {

    $pubDate = new DateTime($t->pubDate);
    $pdate = $pubDate->format('Y-m-d');
    $category = $t->category;
    $show = str_replace('TV Shows / ', '', $category);
    $tv = $t->children($namespaces['torrent']);
    $hash = $tv->infoHash;
    $description = $t->description;
    $link = $tv->magnetURI;
    $yes = preg_match('/Air date:.*/', $description, $matches);

    if (1 === $yes) {
        $airDate = new DateTime(str_replace('Air date: ', '', $matches[0]));
        $adate = $airDate->format('Y-m-d');
    } else {
        $adate = $pdate;
    }

    $tmp_title = str_replace(' [1080p]', '', $t->title);
    $tmp_title = str_replace(' [720p]', '', $tmp_title);
    $tmp_title = $db->quote($tmp_title);

    $sql1 = "SELECT count(1) FROM magnet WHERE hash='{$hash}'";

    $sql2 = "SELECT count(*) FROM (SELECT REPLACE (t_title, ' [1080p]', '') as t_title
                                     FROM magnet
                                    WHERE t_title is not null
                                      AND t_title like '%[1080p]%'
                                    UNION
                                   SELECT REPLACE(t_title, ' [720p]', '')
                                     FROM magnet
                                    WHERE t_title is not null
                                      AND t_title like '%[720p]%')
              WHERE t_title = $tmp_title";

    $stmt = $db->query($sql1);
    $stmt2 = $db->query($sql2);

    if ($stmt->fetchColumn() == 0 && $stmt2->fetchColumn() == 0) {
        // Add magnet link to torrent download
        exec("transmission-remote {$GLOBALS['HOST_PORT']} --add {$link}", $output, $return_code);

        // Implemented quoting
        $show = $db->quote($show);
        $t_title = $db->quote($t->title);

        if ($return_code == 0) {
            $db->query("INSERT INTO magnet (hash, t_show, pub_date, filename, t_title)
                        VALUES ('{$hash}', {$show},'{$adate}','{$link}', {$t_title})");
            echo "Queued and inserted: {$show} ({$hash})\n";
        } else {
            foreach ($output as $l) {
                echo $l, "\n";
            }
        }
    }
}